import React, { Component} from 'react'
import {AsyncStorage} from 'react-native'
import { Provider } from 'react-redux'
import { persistStore, autoRehydrate } from 'redux-persist'
import configureStore from './store/configureStore'
import App from './containers/App'

const store = configureStore()

export default class extends Component {
  render() {
    return (
      <Provider store={store}>
        <App />
      </Provider>
    )
  }
}

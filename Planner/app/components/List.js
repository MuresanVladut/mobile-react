import React, {Component, PropTypes} from 'react'
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  TextInput
} from 'react-native'
import Button from 'react-native-button';
import Modal from 'react-native-animated-modal';
import Checkbox from './Checkbox'

const styles = StyleSheet.create({

  container: {
    flex: 1
  },
  item: {
    padding: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: 'whitesmoke'
  },
  rightSection: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  remove: {
    marginLeft: 10,
    marginBottom: 2,
    color: '#CD5C5C',
    fontSize: 26
  },
  completed: {
    backgroundColor: 'whitesmoke'
  },
  modal: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  wrapper: {
    paddingTop: 50,
    flex: 1
  },
  modal4: {
    height: 400
  },
  btn: {
    margin: 10,
    backgroundColor: "#3B5998",
    padding: 10
  },
  btnModal: {
    position: "absolute",
    top: 0,
    right: 0,
    width: 50,
    height: 50,
    backgroundColor: "transparent"
  },
  text: {
    color: "black",
    fontSize: 18,
    paddingTop: 15
  }

})

export default class List extends Component {
  constructor() {
    super();
    this.state = {
      isModalVisible: false,
      value: ''
    }
  }
  static propTypes = {
    items: PropTypes.array.isRequired,
    onRemoveItem: PropTypes.func.isRequired,
    color: PropTypes.string,
    onToggleItemCompleted: PropTypes.func.isRequired
  }

  openModal4() {
    this.setState({isModalVisible: true});
  }

  editTodo = (i) => {
    const {onEditToDo} = this.props
    onEditToDo(i, this.state.value);
    this.state.isModalVisible = false;
    this.state.value = '';
  }

  _renderModalContent = (i) => {let index = i; return(
    <View style={{
      flexDirection: 'row'
    }}>
      <Text style={styles.text}>Edit:
      </Text>
      <TextInput style={{width: 200, fontSize: 18}}
        placeholder=''
        value={this.state.value}
        onChange={(event) => this.setState({value: event.nativeEvent.text})}></TextInput>
      <Button onPress={_ => {this.editTodo(i)}} style={styles.btn}>Ok</Button>
    </View>
  )}

  renderItem = (item, i) => {
    const {onToggleItemCompleted, onRemoveItem} = this.props
    const itemStyle = item.completed
      ? [styles.item, styles.completed]
      : styles.item

    return (
      <View key={i}>
        <View style={itemStyle}>
          <Text>
            {item.id} - {item.title}
          </Text>
          <View style={styles.rightSection}>
            <Checkbox isChecked={item.completed} onToggle={() => onToggleItemCompleted(i)}/>
            <TouchableOpacity onPress={() => onRemoveItem(i)}>
              <Text style={styles.remove}>
                &times;
              </Text>
            </TouchableOpacity>
            <Button onPress={() => this.openModal4()}>
              <Text>
                Edit
              </Text>
            </Button>
            <Modal
              isVisible={this.state.isModalVisible}
              backdropColor={'lightblue'}
              backdropOpacity={1}
              animationIn={'zoomInDown'}
              animationOut={'zoomOutUp'}
              animationInTiming={1000}
              animationOutTiming={1000}
              backdropTransitionInTiming={1000}
              backdropTransitionOutTiming={1000}>
              {this._renderModalContent(i)}
            </Modal>
          </View>
        </View>
      </View>
    )
  }

  render() {
    const {items} = this.props

    return (
      <ScrollView style={styles.container}>
        {items.map(this.renderItem)}
      </ScrollView>
    )
  }
}

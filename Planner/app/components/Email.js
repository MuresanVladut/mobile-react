import React, { Component, PropTypes } from 'react'
import { TouchableOpacity, Text, StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  footer: {
    paddingVertical: 15,
    alignItems: 'center',
  },
  remove: {
    color: '#CD5C5C',
  },
})

export default class Email extends Component {

  static propTypes = {
    onSendViaEmail: PropTypes.func,
  }

  render() {
    const {onSendViaEmail} = this.props

    return (
      <TouchableOpacity style={styles.footer} onPress={onSendViaEmail}>
        <Text style={styles.remove}>Send via email!</Text>
      </TouchableOpacity>
    )
  }
}

import ToDo from '../model/ToDo';
import _ from 'lodash';


const url = `https://670bdd0e.ngrok.io/todos`;
const types = {
  ADD_ITEM: 'ADD_ITEM',
  REMOVE_ITEM: 'REMOVE_ITEM',
  TOGGLE_ITEM_COMPLETED: 'TOGGLE_ITEM_COMPLETED',
  REMOVE_COMPLETED: 'REMOVE_COMPLETED',
  EDIT_TODO: 'EDIT_TODO',
  CHANGE_CONNECTION_STATUS: 'CHANGE_CONNECTION_STATUS',
  ADD_TO_QUEUE: 'ADD_TO_QUEUE',
  REMOVE_FROM_QUEUE: 'REMOVE_FROM_QUEUE'
}

export const actionCreators = {
  addItem: (item, isConnected) => {
    return {type: types.ADD_ITEM, payload: item}
  },
  removeItem: (index, isConnected) => {
    return {type: types.REMOVE_ITEM, payload: index}
  },
  toggleItemCompleted: (index) => {
    return {type: types.TOGGLE_ITEM_COMPLETED, payload: index}
  },
  removeCompleted: (item) => {
    return {type: types.REMOVE_COMPLETED, payload: item}
  },
  editToDo: (index, value, isConnected) =>{
    return {type: types.EDIT_TODO, payload: {index: index, value: value}}
  },
  connectionState: (status) =>{
    return {type: types.CHANGE_CONNECTION_STATUS, isConnected: status}
  },
  addToQueue: (payload) =>{
    return {type: types.ADD_TO_QUEUE, payload: payload}
  },
  removeFromQueue: (payload) =>{
    return {type: types.REMOVE_FROM_QUEUE, payload: payload}
  }
}

const initialState = {
  items: [],
  queue: []
}

export const reducer = (state = initialState, action) => {
  const {type, payload} = action
  const {items} = state

  switch(type) {
    case types.ADD_ITEM: {
      return {
        ...state,
        items: [...items,payload],
      }
    }
    case types.REMOVE_ITEM: {
      return {
        ...state,
        items: items.filter((item, i) => i !== payload),
      }
    }
    case types.TOGGLE_ITEM_COMPLETED: {
      return {
        ...state,
        items: items.map((item, i) => {
          if (i === payload) {
            // return {label: item.label, completed: !item.completed}
            return new ToDo(item.title, !item.completed, item.id);
          }
          return item
        }),
      }
    }
    case types.REMOVE_COMPLETED: {
      return {
        ...state,
        items: items.filter((item) => !item.completed)
      }
    }
    case types.EDIT_TODO: {
      return {
        ...state,
        items: items.map((item,i) => {
          if(i === payload.index){
            return new ToDo(payload.value, item.completed, item.id);
          }
          return item;
        }),
      }
    }
    case types.CHANGE_CONNECTION_STATUS:{
      return Object.assign({}, state, {
        isConnected: action.isConnected,
      });
    }
    case types.ADD_TO_QUEUE:
      return Object.assign({}, state, {
        queue: state.queue.concat([action.payload]),
      });
    case types.REMOVE_FROM_QUEUE:
      return Object.assign({}, state, {
        queue: _.without(state.queue, action.payload),
      });
    default: {
      return state
    }
  }
}

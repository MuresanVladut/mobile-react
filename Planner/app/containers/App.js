import React, {Component, PropTypes} from 'react'
import {View, StyleSheet, NetInfo, AppState} from 'react-native'
import {connect} from 'react-redux'
import Communications from 'react-native-communications';
import {actionCreators} from '../redux/todoRedux'
import Title from '../components/Title'
import Input from '../components/Input'
import List from '../components/List'
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import Modal from 'react-native-animated-modal';
import Chart from 'react-native-chart';
import Button from 'react-native-button';
import Auth0Lock from 'react-native-lock';
import ToDo from '../model/ToDo';
import PushNotification from 'react-native-push-notification'

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  btn: {
    margin: 10,
    backgroundColor: "#3B5998",
    padding: 10
  },
  divider: {
    height: 1,
    backgroundColor: 'whitesmoke'
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white'
  },
  chartContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white'
  },
  chart: {
    width: 300,
    height: 100
  }
})
const url = `https://8894e5d6.ngrok.io/todos`;
const mapStateToProps = (state) => ({items: state.items, isConnected: state.isConnected, queue: state.queue})

class App extends Component {
  constructor() {
    super();
    this.state = {
      isModalVisible: false
    }
  }
  componentWillMount() {
    NetInfo
      .isConnected
      .addEventListener('change', this.handleConnectionChange);
    var lock = new Auth0Lock({clientId: 'eZBP3xC9hkE8Y4Vy052NwtdsgxNXzqBL', domain: 'yoomury.eu.auth0.com'});
    lock.show({}, (err, profile, token) => {
      if (err) {
        console.log(err);
        return;
      }
      // Authentication worked!
      console.log('Logged in with Auth0!');
      PushNotification.localNotificationSchedule({
        date: new Date(Date.now() + (60 * 1000)),
        // date: new Date(new Date().setHours(20)),
        autoCancel: true, // (optional) default: true
        largeIcon: "ic_launcher", // (optional) default: "ic_launcher"
        smallIcon: "ic_notification", // (optional) default: "ic_notification" with fallback for "ic_launcher"
        bigText: "Hey, you have [" + this.props.items.filter((item) => !item.completed).length + "] uncompleted tasks!", // (optional) default: "message" prop
        color: "red", // (optional) default: system default
        title: "ToDo App", // (optional, for iOS this is only used in apple watch, the title will be the app name on other iOS devices)
        message: "My Notification Message", // (required),
        repeatType: 'day'
      });
    });
  }

  componentWillUnmount() {
    NetInfo
      .isConnected
      .removeEventListener('change', this.handleConnectionChange);
  }

  handleConnectionChange = isConnected => {
    const {dispatch, queue} = this.props
    dispatch(actionCreators.connectionState(isConnected));

    if (isConnected && queue.length > 0) {
      console.log('>0')
      queue.forEach(q => {
        if (q.type === 'ADD') {
          this.addItemToServer(q.item);
        }
        if (q.type === 'REMOVE') {
          this.removeItemFromTheServer(q.item);
        }
        if (q.type === 'EDIT') {
          this.editToServer(q.item.index, q.item.value);
        }
        if (q.type === 'TOGGLE') {
          this.toggleToServer(q.item)
        }
        dispatch(actionCreators.removeFromQueue(q));
      });
    }
  }

  static propTypes = {
    items: PropTypes.array.isRequired,
    dispatch: PropTypes.func.isRequired
  }

  addItem = (item) => {
    const {dispatch} = this.props
    const object = new ToDo(item);
    if (this.props.isConnected) {
      this.addItemToServer(object);
    } else {
      dispatch(actionCreators.addToQueue({type: 'ADD', item: object}));
    }
    dispatch(actionCreators.addItem(object));
  }

  addItemToServer = item => {
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(item)
    }).then((res) => console.log('ADD + ' + res.json())).catch((err) => console.log('EROARE + ' + err));
  }

  removeItem = (index) => {
    const {dispatch} = this.props
    const object = this.props.items[index];
    if (this.props.isConnected) {
      this.removeItemFromTheServer(object.id);
    } else {
      dispatch(actionCreators.addToQueue({type: 'REMOVE', item: object.id}));
    }
    dispatch(actionCreators.removeItem(index, this.props.isConnected));
  }

  removeItemFromTheServer = id => {
    const deleteUrl = url + '/' + id;
    fetch(deleteUrl, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }).then((res) => console.log('REMOVE + ' + res.json())).catch((err) => console.log('EROARE + ' + err));
  }

  toggleItemCompleted = (index) => {
    const {dispatch} = this.props
    const object = this.props.items[index];
    if (this.props.isConnected) {
      this.toggleToServer(object);
    } else {
      dispatch(actionCreators.addToQueue({type: 'TOGGLE', item: object}));
    }
    dispatch(actionCreators.toggleItemCompleted(index))
  }

  editToDo = (index, value) => {
    const {dispatch} = this.props
    const object = this.props.items[index];
    if (this.props.isConnected) {
      this.editToServer(object, value);
    } else {
      dispatch(actionCreators.addToQueue({
        type: 'EDIT',
        item: {
          value: value,
          index: object
        }
      }));
    }
    dispatch(actionCreators.editToDo(index, value, this.props.isConnected))
  }

  toggleToServer = (value) => {
    const putUrl = url + '/' + value.id;
    fetch(putUrl, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(new ToDo(value.title, !value.completed, value.id))
    }).then((res) => console.log('TOGGLE + ' + res.json())).catch((err) => console.log('EROARE + ' + err));
  }

  editToServer = (object, value) => {
    const putUrl = url + '/' + object.id;
    fetch(putUrl, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(new ToDo(value, object.completed, object.id))
    }).then((res) => console.log('EDIT + ' + res.json())).catch((err) => console.log('EROARE + ' + err));
  }

  removeCompleted = () => {
    const {dispatch} = this.props
    dispatch(actionCreators.removeCompleted())
  }

  sendViaEmail = () => {
    var stringList = '';
    let dataList = this.props.items;
    dataList.forEach(function (el) {
      stringList += '[' + el.title + "; Completed: " + el.completed + "] \n";
    });
    Communications.email(['muresan25@gmail.com'], null, null, 'Your Todos', stringList);
  }

  _renderModalContent = () => {
    const data = [
      [
        'Jan', 0
      ],
      [
        'Feb', 0
      ],
      [
        'Mar', 0
      ],
      [
        'Apr', 0
      ],
      [
        'May', 0
      ],
      [
        'Jun', 0
      ],
      [
        'Jul', 0
      ],
      [
        'Aug', 0
      ],
      [
        'Sep', 0
      ],
      [
        'Oct', 0
      ],
      [
        'Nov', 0
      ],
      ['Dec', 0]
    ];

    const {items} = this.props
    for (let model of items) {
      let date = new Date(model.createdAt).getMonth();
      data[date][1]++;
    }
    return (
      <View>
        <Chart
          style={styles.chart}
          data={data}
          verticalGridStep={5}
          type="bar"
          showXAxisLabels={true}
          showYAxisLabels={true}
          showAxis={true}/>
        <Button onPress={this.closeStatistics} style={styles.btn}>Close</Button>
      </View>
    );
  }

  showStatistics = () => {
    this.setState({isModalVisible: true});
  }

  closeStatistics = () => {
    this.setState({isModalVisible: false});
  }

  render() {
    const {items} = this.props

    return (
      <View style={styles.container}>
        <Title>
          Todo List
        </Title>
        <Input placeholder={'Enter an item!'} onSubmit={this.addItem}/>
        <View style={styles.divider}/>
        <List
          items={items}
          onRemoveItem={this.removeItem}
          onToggleItemCompleted={this.toggleItemCompleted}
          onEditToDo={this.editToDo}/>
        <View style={styles.divider}/>

        <ActionButton buttonColor="rgba(231,76,60,1)">
          <ActionButton.Item
            buttonColor='#9b59b6'
            title="Remove Completed"
            onPress={this.removeCompleted}>
            <Icon name="md-remove" style={styles.actionButtonIcon}/>
          </ActionButton.Item>
          <ActionButton.Item
            buttonColor='#9b59b6'
            title="Send todos via email"
            onPress={this.sendViaEmail}>
            <Icon name="md-mail" style={styles.actionButtonIcon}/>
          </ActionButton.Item>
          <ActionButton.Item
            buttonColor='#9b59b6'
            title="Statistics"
            onPress={_ => this.showStatistics()}>
            <Icon name="md-podium" style={styles.actionButtonIcon}/>
          </ActionButton.Item>
        </ActionButton>
        <Modal
          isVisible={this.state.isModalVisible}
          backdropColor={'lightblue'}
          backdropOpacity={1}
          animationIn={'zoomInDown'}
          animationOut={'zoomOutUp'}
          animationInTiming={1000}
          animationOutTiming={1000}
          backdropTransitionInTiming={1000}
          backdropTransitionOutTiming={1000}>
          {this._renderModalContent()}
        </Modal>
      </View>
    )
  }
}

export default connect(mapStateToProps)(App)

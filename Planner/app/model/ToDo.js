var ToDo = (function(){
  var nextId = 1;
  return function ToDo(title, completed, id){
    this.id = id || nextId++;
    this.title = title;
    this.completed = completed || false;
    this.createdAt = new Date();
  }
})();

module.exports = ToDo;